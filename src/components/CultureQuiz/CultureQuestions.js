import React from 'react';
import CorrectAnswerPng from '../../assets/poprawna_odpowiedź_.png';
export let correctAnswer = <img src={CorrectAnswerPng} alt='correct-answer' />;
export const questions = [
    {
        id:1,
        questionText: 'Której z wymienionych oper  NIE stworzył Wolfgang Amadeus Mozart?',
        answerOptions: [
            { id: 1, answerText: 'A.Czarodziejski flet', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Wesele Figara', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Don Giovanni', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Cyrulik sewilski', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:2,
        questionText: 'Most Golden Gate to jeden z najpiękniejszych i najbardziej rozpoznawalnych amerykańskich mostów na świecie. Gdzie się znajduje?',
        answerOptions: [
            { id: 1, answerText: 'A.Nad Zatoką San Francisco', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Nad jeziorem Pontchartrain w Nowym Orleanie', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Na rzece Kolorado, na granicy stanów Arizona i Nevada', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.W Nowym Jorku, łączy dzielnice Brooklyn i Manhattan', isCorrect: false, result: [] },

        ],
    },
    {
        id:3,
        questionText: 'Skąd pochodzi kanapka Francesinha?',
        answerOptions: [
            { id: 1, answerText: 'A.USA', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Portugalia', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Francja', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Gujana Francuska', isCorrect: false, result: [] },
        ],
    },
    {
        id:4,
        questionText: 'Kiedy został wyemitowany pierwszy program radiowy Polskiego Radia?',
        answerOptions: [
            { id: 1, answerText: 'A.20 maja 1946r.', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.1 lutego 1925r.', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.25 kwietnia 1919r.', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.01 stycznia 1958r.', isCorrect: false, result: [] },
        ],
    },
    {
        id:5,
        questionText: 'Jaki kolor mają obecnie świadectwa ukończenia szkoły podstawowej?',
        answerOptions: [
            { id: 1, answerText: 'A.niebieski', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.żółty', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.zielony', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.pomarańczowy', isCorrect: false, result: [] },
        ],
    },
    {
        id:6,
        questionText: 'Kto jest autorem obrazu Ostatnia Wieczerza?',
        answerOptions: [
            { id: 1, answerText: 'A.Raffaello Santi', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Leonardo da Vinci', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Michał Anioł', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Sandro Botticelli', isCorrect: false, result: [] },
        ],
    },
    {
        id:7,
        questionText: 'Najsłynniejszy Portret świata to:',
        answerOptions: [
            { id: 1, answerText: 'A.Dama z gronostajem', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Mona Lisa', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Dziewczyna w perłowych kolczykach', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Dama z łasiczka', isCorrect: false, result: [] },
        ],
    },
    {
        id:8,
        questionText: 'W jakim kraji możemy zobaczyć Pomnik Chrystusa Odkupiciela?',
        answerOptions: [
            { id: 1, answerText: 'A.Chile', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Argentyna', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Wenezuela', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Brazylia', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:9,
        questionText: 'Pierwowzorem Pana Młodego w dramacie „Wesele” Stanisława Wyspiańskiego był:',
        answerOptions: [
            { id: 1, answerText: 'A.Kazimierz Przerwa-Tetmajer', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Jan Kasprowicz', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Stanisław Przybyszewski', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Lucjan Rydel', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:10,
        questionText: 'Kim był Witold Gruca?',
        answerOptions: [
            { id: 1, answerText: 'A.Aktorem', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Pisarzem', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Tańcerzem i choreografem', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Dyrygentem', isCorrect: false, result: [] },
        ],
    },
]

export default { questions, correctAnswer };