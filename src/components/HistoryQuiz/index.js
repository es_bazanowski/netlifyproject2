import React, { useEffect, useState } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';
import BackButton from '../../assets/cofnij_x.png';
import QuitButton from '../../assets/zamknij_x.png';
import ButtonStart from '../../assets/historia-start-button.png';
import { Link } from 'react-router-dom';
import technology_icon from '../../assets/technologia_ikona_.png';
import culture_icon from '../../assets/kultura_ikona.png';
import automotive_icon from '../../assets/motoryzacja_ikona.png';
import programming_icon from '../../assets/programowanie_ikona.png';
import history_icon from '../../assets/historia_ikona.png';
import { questions } from './HistoryQuestions';
import correctAnswerIcon from '../../assets/poprawna_odpowiedź_.png';
import _ from 'lodash';

const HistoryQuiz = (width) => {
    const [score, setScore] = useState(0);
    const [text, setText] = useState(false);
    const [AnswerDrop, setAnswerDrop] = useState([]);
    const [showScore, setShowScore] = useState(false);
    const [correctType, setCorrectType] = useState(false);
    const [Questions, setQuestions] = useState(questions);
    const [currentQuestion, setCurrentQuestion] = useState(0);
    const [resultCorrect, setResultCorrect] = useState('answerResultHide')
    const [mainWrapper, setMainWrapper] = useState('main-quiz-wrapper main-history-background1-quiz')
    const [handleState, setHandleState] = useState(false);
    let correctAnswersList = _.times(score, _.constant(
        <img src={correctAnswerIcon} alt='correctAnswerIcon'></img>));

    const correctAnswersList1 = correctAnswersList.map((list) => <div>{list}</div>)

    function handleOnDragEnd(result) {

        if (!result.destination) return;

        const nextquestion = currentQuestion + 1;
        const items = Array.from(Questions[currentQuestion].answerOptions);
        const [reorderedItem] = Questions[currentQuestion].answerOptions.splice(result.source.index, 1);

        if (nextquestion >= Questions.length) {
            setTimeout(function () { { setShowScore(true) }; }, 1600);
        }
        if (result.destination.droppableId === 'items1') {
            const Answers = Array.from([reorderedItem]);
            setAnswerDrop(Answers);
            setResultCorrect('answerResultShow answerHistoryResultColor');
            setTimeout(function () { { setAnswerDrop([]) } { setCurrentQuestion(nextquestion); } { setResultCorrect('answerResultHide') }; Questions[currentQuestion].answerOptions.splice(result.destination.index, 0, reorderedItem); }, 1600);

            if (reorderedItem.isCorrect === true) {
                setScore(score + 1);
            }
        }

        if (result.destination.droppableId === 'items2') {
            Questions[currentQuestion].answerOptions.splice(result.destination.index, 0, reorderedItem)
        }
    }
    function refreshPage() {
        window.location.reload();
    }

    let intFrameWidth = window.innerWidth;
    const [direction, setDirection] = useState('horizontal');

    useEffect(() => {
        if (intFrameWidth < 1126) {
            setDirection('vertical')
        }
        if (handleState === true) {
            setTimeout(() => { setMainWrapper('main-quiz-wrapper main-history-background2-quiz') }, 1600);
        }
    })

    const handleAnswerClick = (isCorrect) => {
        setResultCorrect('answerResultShow answerHistoryResultColor')
        setHandleState(true);
        if (isCorrect) {
            setScore(score + 1);
        }
        const nextquestion = currentQuestion + 1;
        if (nextquestion < Questions.length) {
            setTimeout(function () {
                setCorrectType(true); {
                    setCurrentQuestion(nextquestion); {
                        setResultCorrect('answerResultHide')
                    }; {
                        setText(true)
                    };
                }
            }, 1600);
        }
    };


    return (
        <div>
            {showScore ? (
                <>
                    <div className='main-quiz-result-wrapper main-history-background'>
                        <div className='quiz-result-header'>
                            <div className='header__icon'><span>Q</span></div></div>
                        <div className='main-quiz-result-content-wrapper'>
                            <div className='quiz-result-summary'>
                                <div className='summary__heading'><div><span>QUIZ</span></div></div>
                                <div className='summary__categoryIcon'><img src={history_icon} alt='history_icon'></img></div>
                                <div className='summary__decorationLine history-summary__decorationLine'></div>
                                <div className='summary__categoryText'>HISTORIA</div>
                                <div className='summary-score'>
                                    <div className='summary-score__scoreText history-summary__scoreText'><span>TWÓJ WYNIK</span></div>
                                    <div className='summary-score__scoreNumber history-summary__scoreNumber'><div>{score}/{Questions.length}</div></div>
                                </div>
                                <div onClick={refreshPage} className='summary-tryAgain history-summary-tryAgain'>
                                    <div className='summary-tryAgain__btnText'>POWTÓRZ TEST</div>
                                    <div className='summary-tryAgain__btnIcon'><img src={ButtonStart} alt='start_icon'></img></div>
                                </div>
                            </div>
                            <div className='quiz-result-categoryList'>
                                <div className='categoryList-GoTo'>
                                    <div className='categoryList-GoTo__actionBtn'><Link to='/history'><img src={BackButton} alt='back_button' /></Link></div>
                                    <div className='categoryList-GoTo__actionBtn'><Link to='/'><img src={QuitButton} alt='quit_button' /></Link></div></div>
                                <div className='categoryList__heading'>WYBIERZ INNĄ KATEGORIĘ:</div>
                                <div className='categoryList-quizList'>
                                    <Link to='/culture'><div className='categoryList-quizList__QuizBtn history-quizList_QuizBtn'>
                                        <div><img src={technology_icon} alt='culture_icon' /></div>
                                        <div className='categoryList-quizList__QuizBtn--decorationLine history-summary__decorationLine'></div>
                                        <div>TECHNOLOGIA</div>
                                    </div>
                                    </Link>
                                    <Link to='/automotive'><div className='categoryList-quizList__QuizBtn  history-quizList_QuizBtn'>
                                        <div><img src={culture_icon} alt='culture_icon' /></div>
                                        <div className='categoryList-quizList__QuizBtn--decorationLine history-summary__decorationLine'></div>
                                        <div>KULTURA</div>
                                    </div>
                                    </Link>
                                    <Link to='/programming'><div className='categoryList-quizList__QuizBtn  history-quizList_QuizBtn'>
                                        <div><img src={automotive_icon} alt='automotive_icon' /></div>
                                        <div className='categoryList-quizList__QuizBtn--decorationLine history-summary__decorationLine'></div>
                                        <div>MOTORYZACJA</div>
                                    </div>
                                    </Link>
                                    <Link to='/history'><div className='categoryList-quizList__QuizBtn  history-quizList_QuizBtn'>
                                        <div><img src={programming_icon} alt='programming_icon' /></div>
                                        <div className='categoryList-quizList__QuizBtn--decorationLine history-summary__decorationLine'></div>
                                        <div>PROGRAMOWANIE</div>
                                    </div>
                                    </Link>
                                </div></div>
                        </div>
                    </div>
                </>
            ) : (
                <div className={mainWrapper}>
                    <div className='main-quiz-wrapper__icon'><span>Q</span></div>
                    <div className='main-quiz-wrapper-header'><div><h1>QUIZ</h1></div>
                        <div className='header__btn'><Link to='/history'><img src={BackButton} alt='back_button' /></Link></div>
                        <div className='header__btn'><Link to='/'><img src={QuitButton} alt='quit_button' /></Link></div>
                    </div>
                    {text ? (<>
                        <div className='main-quiz-wrapper-task history-task' ><div>DRAG &amp; DROP THE RIGHT ANSWER</div>
                            <div className='task__currentTask history__currentTask'>&nbsp;<span>{currentQuestion + 1}/</span><span>10</span></div>
                        </div>
                    </>) : (<>
                        <div className='main-quiz-wrapper-task history-task'><div>SELECT THE CORRECT ANSWER</div>
                            <div className='task__currentTask history__currentTask'> &nbsp;<span>{currentQuestion + 1}/</span><span>10</span></div>
                        </div>
                    </>)}
                    <div className='main-quiz-wrapper__question'><div><span>Q</span></div>{currentQuestion + 1}. {Questions[currentQuestion].questionText}</div>
                    <div className='main-quiz-wrapper-answers'>
                        {correctType ? (<>
                            <DragDropContext onDragEnd={handleOnDragEnd}>
                                <Droppable droppableId='items1'
                                    direction='vertical'>
                                    {(provided, snapshot) => {
                                        return (
                                            <div className='answers-DropAnswer' >
                                                <div
                                                    ref={provided.innerRef}
                                                    {...provided.droppableProps}
                                                    className='answers-DropAnswer-box history-DropAnswer'
                                                >
                                                    <span>Tutaj wrzuć odpowiedź</span>
                                                    <div className='DropAnswer-box__icons'><div className='DropAnswer-box__iconsList'>{correctAnswersList1}</div></div>
                                                    {AnswerDrop.map(({ id, answerText, isCorrect, result }, index) => {
                                                        return (
                                                            <Draggable
                                                                key={`draggable1-${id}`}
                                                                draggableId={`draggable1-${id}`}
                                                                index={index}>
                                                                {(provided, snapshot) => {
                                                                    return (
                                                                        <div
                                                                            ref={provided.innerRef}
                                                                            {...provided.draggableProps}
                                                                            {...provided.dragHandleProps}
                                                                        >
                                                                            <div className='answers-chooseAnswer history-chooseAnswer'>
                                                                                <div className='answers-chooseAnswer__answerText'>{answerText}</div><div className={resultCorrect}>{result}</div>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                }}
                                                            </Draggable>
                                                        )
                                                    })}
                                                    {provided.placeholder}
                                                </div>
                                            </div>
                                        );
                                    }}
                                </Droppable>
                                <Droppable droppableId='items2'
                                    direction={direction}>
                                    {(provided, snapshot) => {
                                        return (
                                            <div
                                                ref={provided.innerRef}
                                                {...provided.droppableProps}
                                                className='answers-wrapper'
                                            >
                                                {Questions[currentQuestion].answerOptions.map(({ id, answerText, isCorrect, result }, index) => {
                                                    return (
                                                        <Draggable
                                                            key={`draggable2-${id}`}
                                                            draggableId={`draggable2-${id}`}
                                                            index={index}>
                                                            {(provided, snapshot) => {
                                                                return (
                                                                    <div
                                                                        ref={provided.innerRef}
                                                                        {...provided.draggableProps}
                                                                        {...provided.dragHandleProps}
                                                                    >
                                                                        <div className='answers-chooseAnswer history-chooseAnswer'>
                                                                            <div className='answers-chooseAnswer__answerText'>{answerText}</div><div className={resultCorrect}>{result}</div>
                                                                        </div>
                                                                    </div>
                                                                )
                                                            }}
                                                        </Draggable>
                                                    )
                                                })}
                                                {provided.placeholder}
                                            </div>
                                        );
                                    }}
                                </Droppable>
                            </DragDropContext></>) : (<>
                                <div className='answers-wrapper'>
                                    {Questions[currentQuestion].answerOptions.map(({ id, answerText, isCorrect, result }, index) => (
                                        <div key={id} className='answers-chooseAnswer history-chooseAnswer' onClick={() => handleAnswerClick(isCorrect)}>
                                            <div className='answers-chooseAnswer__answerText'>{answerText}</div><div className={resultCorrect}>{result}</div>
                                        </div>
                                    ))}
                                </div>
                            </>)}
                    </div>
                </div>
            )}
        </div>
    );
}

export default HistoryQuiz;
