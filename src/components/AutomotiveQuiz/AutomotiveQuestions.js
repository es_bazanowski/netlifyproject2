import React from 'react';
import CorrectAnswerPng from '../../assets/poprawna_odpowiedź_.png';
export let correctAnswer = <img src={CorrectAnswerPng} alt='correct-answer' />;
export const questions = [
    {
        id:1,
        questionText: 'Kto wymyślił logo Alfa Romeo?',
        answerOptions: [
            { id: 1, answerText: 'A.Alexandre Darracq', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Romano Cattaneo', isCorrect: true, result: (correctAnswer) },
            { id: 3, answerText: 'C.Nicola Romeo', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Don Giovanni', isCorrect: false, result: [] },
        ],
    },
    {
        id:2,
        questionText: 'W którym roku pojawiły się pierwsze obowiązkowe tablice rejestracyjne?',
        answerOptions: [
            { id: 1, answerText: 'A.1893', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.1900', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.1934', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.1980', isCorrect: false, result: [] },

        ],
    },
    {
        id:3,
        questionText: 'Od czego pochodzi skrót BMW?',
        answerOptions: [
            { id: 1, answerText: 'A.British Motor Sport', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.British MotorW.', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Bayerisch Motor Werke', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Bayerische Motoren Werke', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:4,
        questionText: 'Mikrus to nazwa:',
        answerOptions: [
            { id: 1, answerText: 'A.Hulajnogi z napędem elektrycznym.', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Samochodzika na akumulator', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Polskiego samochodu osobowego', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Skutera', isCorrect: false, result: [] },
        ],
    },
    {
        id:5,
        questionText: 'Który z samochodów Polski FIAT był największy?',
        answerOptions: [
            { id: 1, answerText: 'A.125p', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.127p', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.126p', isCorrect: false, result: [] },
        ],
    },
    {
        id:6,
        questionText: '"Ogórek" to potoczna nazwa autobusu:',
        answerOptions: [
            { id: 1, answerText: 'A.Jelcz', isCorrect: true, result: (correctAnswer) },
            { id: 2, answerText: 'B.Ikarus', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Autosan', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.San', isCorrect: false, result: [] },
        ],
    },
    {
        id:7,
        questionText: '"Osa" to nazwa:',
        answerOptions: [
            { id: 1, answerText: 'A.Motocykla', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Motoroweru', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Roweru', isCorrect: false, result: [] },
            { id: 4, answerText: 'D.Skutera', isCorrect: true, result: (correctAnswer) },
        ],
    },
    {
        id:8,
        questionText: 'Który motocykl nie był produkowany w PL?',
        answerOptions: [
            { id: 1, answerText: 'A.Junak', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.MZ', isCorrect: false, result: (correctAnswer) },
            { id: 3, answerText: 'C.SHL', isCorrect: false, result: [] },
        ],
    },
    {
        id:9,
        questionText: 'Skrótem "japońska manufaktura" nazwano markę:',
        answerOptions: [
            { id: 1, answerText: 'A.Mazda', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Toyota', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Nissan', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Subaru', isCorrect: false, result: [] },
        ],
    },
    {
        id:10,
        questionText: 'Jaki kształt ma znak "STOP"?',
        answerOptions: [
            { id: 1, answerText: 'A.Okrągły', isCorrect: false, result: [] },
            { id: 2, answerText: 'B.Trójkątny', isCorrect: false, result: [] },
            { id: 3, answerText: 'C.Inny', isCorrect: true, result: (correctAnswer) },
            { id: 4, answerText: 'D.Kwadratowy', isCorrect: false, result: [] },
        ],
    },
]

export default { questions, correctAnswer };