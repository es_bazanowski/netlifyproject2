import QuizList from '../../components/quizList';

const Homepage = () => {

    return (
        <div className='homepage_content_wrapper '>
            <div className='homepage_content_wrapper0'><span>Q</span></div>
            <div className='homepage_content_wrapper1'><h1>QUIZ</h1></div>
            <div className='homepage_content_wrapper2'><p>10 PYTAŃ/ 5 KATEGORII</p></div>
            <div className='homepage_content_wrapper3'><QuizList /></div>

        </div>
    )
}

export default Homepage;