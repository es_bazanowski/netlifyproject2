import automotive_icon from '../../assets/motoryzacja_ikona.png'
import button_pic from '../../assets/culture_button-strzalka.png'
import BackButton from '../../assets/cofnij_x.png'
import QuitButton from '../../assets/zamknij_x.png'
import { Link } from 'react-router-dom';
const Automotive = () => {
    return (
        <div className='selected-category-wrapper main-automotive-background'>
            <div className='selected-category-wrapper__icon'><span>Q</span></div>
            <div className='selected-category-wrapper-header'><div><h1>QUIZ</h1></div>
                <div className='header_btn'><Link to='/'><img src={BackButton} alt='back_button' /></Link></div>
                <div className='header_btn'><Link to='/'><img src={QuitButton} alt='quit_button' /></Link></div>
            </div>
            <div className='selected-category-wrapper__selCategory pickCat_automotive_background'><p>WYBRANA KATEGORIA:</p></div>
            <div className='selected-category-wrapper__category'><div className='main-category_wrapper'>
                <div className='category__icon'><img src={automotive_icon} alt='automotive_icon' /></div>
                <div className='category__line automotive-summary__decorationLine'></div>
                <div className='category__text'>MOTORYZACJA</div>
                <Link to='/AutomotiveQuiz'><div className='category__btn automotive-start-button-background'>
                    <div><p>ROZPOCZNIJ</p></div>
                    <div className='category__start automotive-category__start'></div>
                </div></Link>
            </div></div>

        </div>
    )
}

export default Automotive;