import culture_icon from '../../assets/kultura_ikona.png'
import BackButton from '../../assets/cofnij_x.png'
import QuitButton from '../../assets/zamknij_x.png'
import { Link } from 'react-router-dom';
const Culture = () => {
    return (
        <div className='selected-category-wrapper main-culture-background'>
            <div className='selected-category-wrapper__icon'><span>Q</span></div>
            <div className='selected-category-wrapper-header'><div><h1>QUIZ</h1></div>
                <div className='header_btn'><Link to='/'><img src={BackButton} alt='back_button' /></Link></div>
                <div className='header_btn'><Link to='/'><img src={QuitButton} alt='quit_button' /></Link></div>
            </div>
            <div className='selected-category-wrapper__selCategory pickCat_culture_background'><p>WYBRANA KATEGORIA:</p></div>
            <div className='selected-category-wrapper__category'><div className='main-category_wrapper'>
                <div className='category__icon'><img src={culture_icon} alt='culture_icon' /></div>
                <div className='category__line culture-summary__decorationLine'></div>
                <div className='category__text'>KULTURA</div>
                <Link to='/cultureQuiz'><div className='category__btn culture-start-button-background'>
                    <div><p>ROZPOCZNIJ</p></div>
                    <div className='category__start culture-category__start'></div>
                </div></Link>
            </div></div>

        </div>
    )
}

export default Culture;