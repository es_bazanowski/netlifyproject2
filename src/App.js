import { BrowserRouter as Router, Switch, Route, } from 'react-router-dom';
import { createBrowserHistory } from 'history'
import Homepage from './pages/homepage';
import Technology from './pages/technology';
import Automotive from './pages/automotive';
import Culture from './pages/culture';
import Programming from './pages/programming';
import History from './pages/history';
import TechnologyQuiz from './components/TechnologyQuiz';
import CultureQuiz from './components/CultureQuiz';
import AutomotiveQuiz from './components/AutomotiveQuiz';
import ProgrammingQuiz from './components/ProgrammingQuiz';
import HistoryQuiz from './components/HistoryQuiz';
import './styles/themes/default/theme.scss';
function App() {
  return (

    <Router>
      <Switch>
        <Route exact path='/' component={Homepage} />
        <Route path='/culture' component={Culture} />
        <Route path='/technology' component={Technology} />
        <Route path='/automotive' component={Automotive} />
        <Route path='/programming' component={Programming} />
        <Route path='/history' component={History} />
        <Route path='/techQuiz' component={TechnologyQuiz} />
        <Route path='/cultureQuiz' component={CultureQuiz} />
        <Route path='/automotiveQuiz' component={AutomotiveQuiz} />
        <Route path='/programmingQuiz' component={ProgrammingQuiz} />
        <Route path='/historyQuiz' component={HistoryQuiz} />
        <Router history={createBrowserHistory}>
          <Route path="/" component={App} />
        </Router>
      </Switch>
    </Router>

  );
}

export default App;
